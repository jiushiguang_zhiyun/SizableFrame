#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QMouseEvent>
#include <QPoint>
#include <QCursor>
#include <QRect>
#define PADDING 10

namespace Ui {
class Dialog;
}
enum Direction{
    UP = 0,
    DOWN=1,
    LEFT,
    RIGHT,
    LEFTTOP,
    LEFTBOTTOM,
    RIGHTBOTTOM,
    RIGHTTOP,
    NONE
};
class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
private:
    void region(const QPoint &cursorPoint);
protected:
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);
private slots:
    void on_pushButton_clicked();

private:
    bool isLeftPressDown;// 判断左键是否按下
    QPoint dragPosition;// 窗口移动拖动时的鼠标点
    Direction dir;// 窗口大小改变时，记录改变方向
private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
